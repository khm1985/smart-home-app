import { SeluxitApi } from "../api/SeluxitApi";

export class SeluxitService {
  static get(): Promise<any> {
		return new Promise((resolve, reject) => {
			SeluxitApi.get().then((res) => {
				resolve(res.data);
			}).catch((err) => {
				reject(err.response);
			});
		});
  }

  static getDeviceById(id: string): Promise<any> {
		return new Promise((resolve, reject) => {
			SeluxitApi.getDeviceById(id).then((res) => {
				resolve(res.data);
			}).catch((err) => {
				reject(err.response);
			});
		});
  }

  static updateDevice(deviceId: string, valueId: string, stateId: string, data: any): Promise<any> {
		return new Promise((resolve, reject) => {
			SeluxitApi.updateDevice(deviceId, valueId, stateId, data).then((res) => {
				resolve(res.data);
			}).catch((err) => {
				reject(err.response);
			});
		});
  }
}