import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: "/smart-home-app/",
  routes: [
    {
      path: "/",
      name: "root",
      redirect: { path: "/home" },
      meta: {
        visibleDesktop: false,
        visibleMobile: false,
        icon: ""
      }
    },
    {
      path: "/home",
      name: "home",
      component: () =>
        import(/* webpackChunkName: "home" */ "./views/Home"),
      meta: {
        visibleDesktop: true,
        visibleMobile: true,
        icon: "mdi-home-automation"
      }
    },
    {
      path: "/devices",
      name: "devices",
      component: () =>
        import(/* webpackChunkName: "devices" */ "./views/Devices"),
      meta: {
        visibleDesktop: true,
        visibleMobile: true,
        icon: "mdi-devices"
      }
    },
    {
      path: "/profile",
      name: "profile",
      component: () =>
        import(/* webpackChunkName: "profile" */ "./views/Profile"),
      meta: {
        visibleDesktop: false,
        visibleMobile: true,
        icon: "mdi-account-circle"
      }
    },
    {
      path: "/rooms",
      name: "rooms",
      component: () =>
        import(/* webpackChunkName: "rooms" */ "./views/Rooms"),
      meta: {
        visibleDesktop: true,
        visibleMobile: false,
        icon: "mdi-animation"
      }
    },
    {
      path: "/actions",
      name: "actions",
      component: () =>
        import(/* webpackChunkName: "actions" */ "./views/Actions"),
      meta: {
        visibleDesktop: true,
        visibleMobile: false,
        icon: "mdi-gesture-tap"
      }
    },
    {
      path: "/recipes",
      name: "recipes",
      component: () =>
        import(/* webpackChunkName: "recipes" */ "./views/Recipes"),
      meta: {
        visibleDesktop: true,
        visibleMobile: false,
        icon: "mdi-clipboard-play-outline"
      }
    }
  ]
});