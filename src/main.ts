import Vue from "vue";
import VueNativeSock from "vue-native-websocket";
import router from "./router";
import "@mdi/font/scss/materialdesignicons.scss";
import store from "./store/Index";
import App from "./App.vue";

const networkID: string = "b8ad0243-0845-4e17-9683-b66ddfda0647";
Vue.use(VueNativeSock, "wss://www.seluxit.com/smarthome/services/2.0/network/" + networkID + "/websocket", {
  format: "json",
  reconnection: true,
  reconnectionAttempts: 5000,
  reconnectionDelay: 300,
  store: store
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount("#app");