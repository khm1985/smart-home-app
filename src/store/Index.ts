
import Vue from "vue";
import Vuex from "vuex";
import { Report } from "@/models/Report";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    socket: {
      isConnected: false,
      message: "",
      reconnectError: false,
      error: "",
      reconnectCount: 0
    }
  },
  mutations:{
    // tslint:disable-next-line:typedef
    SOCKET_ONOPEN (state, event)  {
      Vue.prototype.$socket = event.currentTarget;
      state.socket.isConnected = true;
    },
    // tslint:disable-next-line:typedef
    SOCKET_ONCLOSE (state)  {
      state.socket.isConnected = false;
    },
    // tslint:disable-next-line:typedef
    SOCKET_ONERROR (state, event)  {
      state.socket.error = event;
    },
    // default handler called for all methods
    // tslint:disable-next-line:typedef
    SOCKET_ONMESSAGE (state, message)  {
      state.socket.message = message;
    },
    // mutations for reconnect methods
    // tslint:disable-next-line:typedef
    SOCKET_RECONNECT(state, count) {
      state.socket.reconnectCount = count;
    },
    // tslint:disable-next-line:typedef
    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    },
  },
  actions: {
    // tslint:disable-next-line:typedef
    sendMessage: function(context, message) {
      window.console.log(message);
      Vue.prototype.$socket.send(message as Report);
    }
  }
});