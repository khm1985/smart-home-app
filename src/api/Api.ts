import { default as axios, AxiosInstance } from "axios";

export class Api {
    static baseUrl;

    static get client(): AxiosInstance {
        return axios.create({
            baseURL: this.baseUrl
		});
	}
}