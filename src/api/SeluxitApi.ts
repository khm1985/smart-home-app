import { Api } from "./Api";
import { Network } from "../models/Network";

export class SeluxitApi extends Api {
    static baseUrl = "https://www.seluxit.com/smarthome/services/2.0/network/b8ad0243-0845-4e17-9683-b66ddfda0647";

    static get(): any {
        return SeluxitApi.client.get("");
    }

    static getDeviceById(id: string): any {
        return SeluxitApi.client.get("/device/" + id);
    }

    static updateDevice(deviceId: string, valueId: string, stateId: string, data: any): any {
        return SeluxitApi.client.patch("/device/" + deviceId + "/value/" + valueId + "/state/" + stateId, data);
    }
}