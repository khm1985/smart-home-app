  export interface Meta {
      type: string;
      version: string;
      id: string;
  }

  export interface Report {
      timestamp: Date;
      data: number;
      status_payment: string;
      type: string;
      status: string;
      meta: Meta;
  }