export interface IMeta {
  type: string;
  version: string;
  id: string;
}

export interface INumber {
  unit: string;
  step: number;
  min: number;
  max: number;
}

export interface IState {
  timestamp: any;
  data: any;
  status_payment: string;
  type: string;
  status: string;
  meta: IMeta;
}

export interface IString {
  max: number;
  encoding: string;
}

export interface IValue {
  name: string;
  type: string;
  permission: string;
  status: string;
  number: Number;
  meta: IMeta;
  state: IState[];
  string: String;
}

export interface IDevice {
  name: string;
  manufacturer: string;
  version: string;
  description: string;
  included: string;
  meta: IMeta;
  status: any[];
  value: IValue[];
  product: string;
  serial: string;
  protocol: string;
}

export class Network {
  name: string;
  meta: IMeta;
  device: IDevice[];
}