export interface IState {
  socket: ISocket;
}
export interface ISocket {
  isConnected: boolean;
  message: string;
  reconnectError: boolean;
  error: string;
  reconnectCount: number;
}
